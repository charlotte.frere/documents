Ceci est une note au cas où j'oublie mon fonctionnement.

Soit x le numéro de TD.
Supposons que nous soyons dans le répertoire dédié à la matière

Les TD non corrigés sont nommés : NC/TDx.tex
Les TD corrigés sont nommés TDx.tex
Les sujets de TD sont nommés : orig/TDx.pdf

Les cours originaux sont nommés : orig/Cx.pdf
Les notes de cours sont nommées : Cx.tex
