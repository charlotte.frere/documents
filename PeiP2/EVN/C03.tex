\input{preamble}

\title{Intégrales généralisées}	
\author{ Edgar Bizel }

\begin{document}
	\maketitle
	
	\onehalfspacing

	\section{Notion d'intégrale généralisée}%
	\label{sec:notion_d_integrale_generalisee}
	
	\subsection{Définition}%
	\label{sub:definition}
	
	\begin{defi}
		Soit $\intervallefo{a}{b}$ un intervalle semi-ouvert de $\R$ borné ou non.

		Soit une application $f: \intervallefo{a}{b} \longrightarrow \R $ telle que :
		$\forall y \in \intervallefo{a}{b}, \int_{a}^{y} f( t ) dt \in \R $

		On dit que $f$ est intégrable sur $\intervallefo{a}{b}$ \ssi $\lim_{y \to b}
		\int_{a}^{y} f( t )dt \in \R $

		On dit alors que l'intégrale généralisée $\int_{a}^{b} f( t )dt$ converge, ou
		encore que $f$ est intégrable en $b$.
	\end{defi}

	\subsection{Cas particuliers et remarques}%
	\label{sub:cas_particuliers_et_remarques}
	
	\begin{itemize}
		\item Dire que "$\forall y \in \intervallefo{a}{b}, \int_{a}^{y} f( t )dt \in \R
		$" signifie que $f$ est intégrable sur tout fermé borné inclus dans
		$\intervallefo{a}{b}$.

		\item Si $f$ est continue sur $\intervallefo{a}{b}$ alors $f$ est localement
		intégrable sur $\intervallefo{a}{b}$.

		\item Si $f$ est prolongeable par continuité en $b$ alors $f$ est localement
		intégrable et intégrable sur $\intervalleff{a}{b}$.

		Dans ce cas, il n'y a pas de question relative à la convergence de $\int_{a}^{b}
		f( t )dt $

		\item On peut exprimer séquentiellement la convergence de $\int_{a}^{b} f( t )dt $ :

		Soit $( y_n )_n \in \intervallefo{a}{b}$ telle que $\lim_{n \to +\infty} y_n = b$.
		$\int_{a}^{b} f( t )dt $ converge \ssi la suite réelle $\left( \int_{a}^{y_n} f( t
		)dt  \right)_n$ converge.

		\item La définition est formulée sur $\intervallefo{a}{b}$. On a donc une
		représentation de $f$ de type :
		\begin{itemize}
			\item Premier graphe : fonction croissante et bornée sur
			$\intervallefo{a}{b}, t > b$ : cas où $\intervallefo{a}{b}$ borné.

			\item Deuxième graphe : fonction décroissante sur $\intervallefo{a}{+\infty}$
			: cas où $\intervallefo{a}{b}=\intervallefo{a}{+\infty}$
		\end{itemize}

		Elle s'applique aussi sur les intervalles de type $\intervallefo{a}{b}$ ou
		$\intervalleoo{a}{b}$. Représentation :
		\begin{itemize}
			\item Premier graphe : fonction croissante sur $\intervalleof{-\infty}{b}$ :
			cas où $\intervalleof{a}{b}=\intervalleof{-\infty}{b}$ 

			\item Deuxième graphe : fonction croissante puis décroissante : cas où
				$\intervalleoo{a}{b}=\intervalleoo{-\infty}{+\infty}$.
		\end{itemize}

		\item Pour étudier une intégrale généralisée sur $\intervalleoo{a}{b}$, on procède
		ainsi :
		\begin{enumerate}
			\item On partitionne le domaine : $\intervalleoo{a}{b}=\intervalleof{a}{c}\cup
			\intervalleoo{c}{b}$.

			\item On étudie $\int_{a}^{c} f( t )dt $ puis $\int_{c}^{b} f( t )dt $
		\end{enumerate}
		Il y a donc 2 intégrales généralisées à étudier.
	\end{itemize}

	\subsection{Exercice}%
	\label{sub:exercice}
	
	\begin{exo}
		\textbf{Montrer que les intégrales généralisées convergent} :

		\begin{enumerate}
			\item $\int_{0}^{1} \frac{1}{\sqrt{1-t}} dt $ 

			Posons $f: t \longmapsto \frac{1}{\sqrt{1-t}}$.

			\begin{itemize}
				\item Graphe
				\begin{figure}[H]
					\centering-
					\begin{tikzpicture}
						\begin{axis}[
							domain=0:1,
							axis lines = middle,
							axis equal image,
							width=14cm,
							height=8cm,
							enlargelimits={abs=0.4},
							no markers,
							samples=200,
							restrict y to domain=0:4,
						]
						\addplot[domain=0:1]{1/( sqrt( 1-x ) )};
						\end{axis}
					\end{tikzpicture}
					\caption{Graphe de $f$}
				\end{figure}

				\item Domaine de définition

				$f$ est continue sur $\intervallefo{0}{1}$ donc localement dérivable sur
				$\intervallefo{0}{1}$.

				{ \red $f$ vérifie le cadre d'études de $\int_{0}^{1} f( t )dt $ }

				\item Soit $y \in \intervallefo{0}{1}$.
				\begin{align*}
					\int_{0}^{y} \frac{1}{1-t} dt 
					&= \intval{ -2 \sqrt{1-t}  }{ 0 }{ y } \\ 
					&= 2( 1\sqrt{1-y} ) \\
					\shortintertext{Passons à la limite :}	
					\lim_{y \to 1} 2( 1-\sqrt{1-y} )&=2
				\end{align*}
			\end{itemize}

			\Conclusion : $\int_{0}^{1} \frac{1}{\sqrt{1-t}} $ converge vers 2

			\item $\int_{0}^{+\infty} \frac{1}{1+t^2}dt $

			Posons 
			\begin{align*}
				g: \intervallefo{0}{+\infty} &\longrightarrow \R \\
				t &\longmapsto \frac{1}{1+t^2} 
			.\end{align*}
			
			\begin{itemize}
				\item $g$ est continue sur $\intervallefo{0}{+\infty}$ donc $g$ est
				localement intégrable. C'est à dire : $g$ est intégrable sur tout
				intervalle fermé borné inclus dans $\intervallefo{0}{+\infty}$

				\item Soit $y \in \intervallefo{0}{+\infty}$.
				\begin{align*}
					\int_{0}^{y} g( t )dt &= \int_{0}^{y} \frac{1}{1+t^2} dt  \\
					&= \intval{ \arctan( y ) }{ 0 }{ y } \\
					&= \arctan( y )\\
					\shortintertext{Passons à la limite :}
					\lim_{y \to +\infty} \arctan(y)=\frac{\pi}{2}
				\end{align*}
			\end{itemize}
			\Conclusion : $\int_{0}^{+\infty} \frac{1}{1+t^2}dt $ converge vers
			$\frac{\pi}{2}$.

			\item Soit $\alpha \in \R$, 
			\begin{align*}
				f: \intervallefo{1}{+\infty} &\longrightarrow \R \\
				t &\longmapsto \frac{1}{t^\alpha} 
			.\end{align*}
			Déterminer une condition nécessaire et suffisante pour que $f$ soit intégrable
			en $+\infty$.

			\begin{itemize}
				\item $f$ est continue sur $\intervallefo{1}{+\infty}$ donc localement
				intégrable sur cet intervalle.

				\item Soit $y \in \intervallefo{1}{+\infty}$.
				\begin{align*}
					\int_{1}^{y} f( t )dt &=
					\begin{cases}
						\ln(y) & \text{ si } \alpha=1\\
						\frac{1}{1-\alpha} \left( \frac{1}{y^{\alpha-1}}-1 \right) &
						\text{ sinon }
					\end{cases}
					\shortintertext{Passons à la limite :}
					\shortintertext{$\alpha = 1$}
					\lim_{y \to +\infty} \ln(y)&=+\infty \donc \int_{1}^{\infty}
					\frac{1}{t}dt \text{ \red diverge} \\
					\shortintertext{$\alpha>1$}
					\lim_{y \to +\infty} \frac{1}{1-\alpha} \left(
					\frac{1}{y^{\alpha-1}}-1 \right) &= \frac{1}{\alpha - 1} \donc
					\int_{1}^{\infty} \frac{1}{t^\alpha} dt \text{ \red converge} 
					\shortintertext{$\alpha < 1$}
					\lim_{y \to +\infty} \frac{1}{1-\alpha} \left(
					\frac{1}{y^{\alpha-1}}-1 \right) &= +\infty \donc \text{\red diverge}
				\end{align*}
			\end{itemize}
			\Conclusion : 
			\begin{itemize}
				\item $\int_{1}^{+\infty} \frac{1}{t^\alpha} dt $ est appelée intégrale de
				Riemann en $+\infty$.

				\item $\int_{1}^{+\infty} \frac{1}{t^\alpha}dt $ converge \ssi le
					paramètre $\alpha$ vérifie $\alpha>1$.
			\end{itemize}
		\end{enumerate}
	\end{exo}

	\subsection{Critère de Cauchy}%
	\label{sub:critere_de_chauchy}
	
	\begin{theo}
		\begin{itemize}
			\item Soit $a \in \R, b \in \R\cup \set{ +\infty } $ 
			\item Soit $f$ localement intégrable sur $\intervallefo{a}{b}$.

			$f$ est intégrable dans en $b \ssi \forall \epsilon >0, \exists c \in
			\intervallefo{a}{b}, \forall u, v \in \R, c \le u \le v < b \implies
			\abs{ \int_{u}^{v} f( t )dt  } < \epsilon$
		\end{itemize}
	\end{theo}
	Illustration :
	\begin{figure}[H]
		\centering
		\begin{tikzpicture}
			% Hachure
			\pgfdeclarepatternformonly{north east lines wide}%
			{\pgfqpoint{-1pt}{-1pt}}%
			{\pgfqpoint{10pt}{10pt}}%
			{\pgfqpoint{9pt}{9pt}}%
			{
				\pgfsetlinewidth{0.4pt}
				\pgfpathmoveto{\pgfqpoint{0pt}{0pt}}
				\pgfpathlineto{\pgfqpoint{9.1pt}{9.1pt}}
				\pgfusepath{stroke}
			}

			\begin{axis}[
				domain=0:5,
				axis lines = middle,
				axis equal image,
				width=14cm,
				enlargelimits={abs=0.4},
				no markers,
				samples=200,
				restrict y to domain=0:3,
			]
				\addplot[domain=0:5]{1/x};
				\addplot+[mark=none,domain=4:5,samples=100,%
					pattern=north east lines wide,%
					pattern color=red!50!yellow]%
					{1/x}
					\closedcycle;
			\end{axis}
		\end{tikzpicture}
	\end{figure}
	\TODO

	\begin{demo}
		Le critère de Cauchy est nécessaire à la convergence de $\int_{a}^{b} f( t )dt $ 

		\highlighty{ Supposons $\int_{a}^{b} f( t )dt $ convergente }. Donc $\lim_{y \to
		b} \int_{a}^{y} f( t )dt=l \in \R $ où $l=\int_{a}^{b} f( t )dt $

		C'est à dire : pour $\epsilon >0$ fixé, il existe une borne $c \in
		\intervallefo{a}{b}$ telle que : $\forall y \in \R, c \le y<b\implies
		\abs{\int_{a}^{y} f( t )dt-l } < \epsilon$

		Considérons $u,v \in \R$ tels que $c \le u<v<b$.
		\begin{align*}
			\abs{\int_{u}^{v} f( t )dt }&= \abs{\int_{u}^{a} f( t )dt+\int_{a}^{v} f( t )dt
			} \\
			&= \abs{\int_{u}^{a} f( t )dt +l - l  +\int_{a}^{v} f( t )dt}, l =
			\int_{a}^{b} f( t )dt  \\
			\shortintertext{Inégalité triangulaire}
			&\le  \abs{\int_{a}^{b} f( t )dt-\int_{a}^{u} f( t )dt  }+ \abs{\int_{a}^{b}
			f( t )dt-\int_{a}^{v} f( t )dt  }\\
			\shortintertext{$u, v \in \intervallefo{c}{b}$}
			&< 2\epsilon
		\end{align*}

		Supposons, pour $\epsilon>0$ fixé, qu'il existe une borne $c \in
		\intervallefo{a}{b}$ telle que : $\forall u, v \in \R, c \le u< v < b \implies
		\abs{\int_{u}^{v} f( t )dt } <\epsilon$  

		En particulier : $\forall v \in \R, c < v < b \implies \abs{\int_{c}^{v} f( t
		)dt}<\epsilon$

		Donc : $\forall v \in \R, c < v < b \implies -\epsilon < \int_{c}^{v} f( t
		)dt<\epsilon $ 

		Donc : $\forall  v \in \R, v < v < b \implies -\epsilon + \underbrace{\int_{a}^{c}
		f( t )dt}_{\in \R} < \int_{a}^{v} f( t )dt < \epsilon+\underbrace{\int_{a}^{c} f(
	t )dt}_{\in \R}$ car $\intervalleff{a}{c}$ 
		fermé borné.

		$c$ dépend de $\epsilon$, qui est choisi aussi petit que voulu. Ce qui exprime
		$\lim_{v \to b} \int_{a}^{v} f( t )dt\in \R $ 

		\textbf{A retenir} : le critère de Cauchy est le plus souvent utiliser pour
		montrer la divergence d'une intégrale:
		\begin{align*}
			\exists  \epsilon > 0, \forall c \in \intervallefo{a}{b}, \exists u, v \in \R,
			c \le u < v < b \et \abs{\int_{u}^{v} f( t )dt } \implies\epsilon \iff
			\int_{a}^{b} f( t )dt \not\in \R  
		\end{align*}
		En pratique : 
		S'il existe un seuil $\epsilon >0$ tel que on peut exhiber une fermé borné
		$\intervalleff{u}{v}$ tel que $ \abs{\int_{u}^{v} f( t )dt } \ge \epsilon$ alors
		$f$ non intégrable en $k$ (et réciproquement)
	\end{demo}

	\subsection{Exemples relatifs au critère de Cauchy}%
	\label{sub:exemples_relatifs_au_critere_de_cauchy}
	
	\begin{enumerate}
		\item 
		\begin{align*}
			f: \intervalleoo{1}{+\infty} &\longrightarrow \R \\
			t &\longmapsto \frac{1}{t\ln(t)} 
		.\end{align*}
		
		Soit $u \in \intervalleoo{1}{+\infty}$.
		\begin{align*}
			\int_{u}^{u^2} f( t )dt &= \int_{u}^{u^2} \frac{1}{t} \frac{1}{\ln(t)}, \text{
			forme :} \frac{g'}{g} \\ 
			&= \intval{ \ln(\abs{ \ln(t) }) }{ u }{ u^2 } \\
			&= \ln(\ln(u^2))-\ln(\ln(u)) \\
			&= \ln\left(\frac{2\ln(u)}{\ln(u)}\right) \\
			&= \ln(2)
		\end{align*}

		Que peut on en déduire pour $\int_{1}^{+\infty} f( t )dt $ ?

		$\int_{1}^{e} f( t )dt $ est elle convergente ? Non, car le critère de Cauchy
		n'est pas vérifié. 

		Pour $\epsilon = \frac{\ln(2)}{n}$, on sait qu'il suffit de choisir  $
		\left\{
		\begin{NiceMatrix}
			u=1+\frac{1}{n}\\
			v=\left( 1+\frac{1}{n} \right)^2
		\end{NiceMatrix}
		\right., n \in \N^*
		$

		On sait à présent que $\int_{1}^{e} f( t )dt $ diverge donc $\int_{1}^{+\infty}
		f( t )dt $ diverge

		De même $\int_{n}^{n^2} f( t )dt =\ln(2)$. Donc, pour $\epsilon=\frac{\ln(2)}{n}$,
		on a toujours : $ \abs{\int_{n}^{n^2} f( t )dt\ge \epsilon }$ 

		Donc $\int_{e}^{+\infty} f( t )dt $ diverge aussi

		\item $\cos, \sin$ ne sont pas intégrables en $+\infty$.

		En effet, pour $\epsilon=\frac{1}{n} ( n\ge 1 ), \abs{\int_{2n\pi}^{( 2n+1
		)\pi} \sin(t)dt }= \abs{\intval{ -\cos(t) }{ 2n\pi }{ ( 2n+1 )\pi }} = 2 \ge
		\epsilon$

		Le critère de Cauchy n'est donc pas vérifié. Donc $\int_{0}^{+\infty} \sin(t)dt $ 
		diverge.
	\end{enumerate}

	\section{Cas des fonctions ne changeant pas de signe}%
	
	\subsection{Préambule}%

	\begin{itemize}
		\item Les théorèmes sont énoncés pour les fonctions positives
		\item Si une application $f$ est à valeurs négatives, considérer $-f$ 
		Par linéarité du nombre intégrale, on peut conclure sur l'intégrabilité ou
		non de $f$
	\end{itemize}
	
	\subsection{Théorème de comparaison}%

	\begin{theo}
		\textbf{Théorème de comparaison}.

		Soient $a \in \R, b \in \R \cup \set{ +\infty } $

		Soient $f,g$ localement intégrables sur $\intervallefo{a}{b}$ 

		S'il existe $c \in \intervallefo{a}{b}$ telle que : $\forall t \in
		\intervallefo{c}{b}, 0 \le g( t ) \le f( t )$ alors : 
		\begin{itemize}
			\item si $\int_{a}^{b} g( t )dt $ diverge alors $\int_{a}^{b} f( t )dt$
			diverge
		\item si $\int_{a}^{b} f( t )dt $ converge alors $\int_{a}^{b} g( t )dt $ converge
		\end{itemize}
	\end{theo}
	\begin{demo}
		\begin{itemize}
			\item Supposons qu'il existe une borne $c \in \intervallefo{a}{b}$ telle que :
			\begin{itemize}
				\item $g$ est à valeurs positives sur $\intervallefo{c}{b}$ 
				\item $g$ est inférieure à $f$ sur $\intervallefo{c}{b}$, c'est à dire $g$
					minore.
			\end{itemize}
			\item Considérons une suite $( b_n )_n$ de $\intervallefo{a}{b}$ telle que :
			\begin{itemize}
				\item $( b_n )_n$ est croissante
				\item $( b_n )_n$ est convergente vers $b$
			\end{itemize}

			On définit alors 2 suites de nombres intégraux, de terme général :
			\begin{align*}
				F( b_n ) = \int_{c}^{b_n}  f( t )dt \et G( b_n )= \int_{c}^{b_n} g( t )dt  
			\end{align*}
			Pour $n$ fixé, $F( b_n )$ et $G( b_n )$ sont définis car $f$ et $g$ sont intégrées
			sur le fermé borné $\intervalleff{c}{b_n}$ 

			\item Supposons que $\int_{a}^{b} g( t )dt $ diverge

			Donc la suite $( G( b_n ) )_n$ ne vérifie pas le critère de Cauchy. On sait :
			\begin{itemize}
				\item $( G( b_n ) )_n$ positive car $g$ positive sur $\intervallefo{c}{b}$
				\item $( G( b_n ) )_n$ croissante
				\item $( G( b_n ) )_n$ minore $( F( b_n ) )_n$	
			\end{itemize}
			Donc la suite $( F( b_n ) )_n$ ne vérifie pas le critère de Cauchy
		\end{itemize}
		On a montré : s'il existe $c \in \intervallefo{a}{b}, \forall t \in
		\intervallefo{c}{b}, 0 \le g( t )\le f( t )$ alors : si $\int_{a}^{b} g( t )dt $ 
		diverge alors $\int_{a}^{b} f( t )dt $ diverge

		Par contraposée de la condition nécessaire, on obtient : si $\int_{a}^{b} f( t )dt
		$ converge alors $\int_{a}^{b} g( t )dt $ converge
	\end{demo}

	\begin{exo}
		Étudier les intégrales généralisées suivantes :
		\begin{enumerate}
			\item $\int_{1}^{+\infty} \frac{\ln(t)}{t}dt $ 

			\begin{align*}
				f: \intervallefo{1}{+\infty} &\longrightarrow \R \\
				t &\longmapsto \frac{\ln(t)}{t} 
			.\end{align*}
			est :
			\begin{itemize}
				\item localement intégrable car $f$ est continue sur
				$\intervallefo{1}{+\infty}$
				\item à valeurs positives
			\end{itemize}
			Pour $t \in \intervallefo{e}{+\infty}$ on a : $\ln(t)\ge \ln(e)$ donc
			$\frac{\ln(t)}{t}\ge \frac{1}{t} >0	$

			Appliquons le théorème de comparaison : $\int_{1}^{\infty}  \frac{dt}{t}$
			diverge donc $\int_{1}^{\infty} \frac{\ln(t)}{t}dt $ diverge

			\item $\int_{-\infty}^{\infty} e^{-t^2} dt $ 

			\begin{align*}
				f: \R &\longrightarrow \R \\
				t &\longmapsto e^{-t^2} 
			.\end{align*}
			est 
			\begin{itemize}
				\item localement intégrable car continue sur $\R$ 
				\item à valeurs positives
				\item paire sur $\R$	
			\end{itemize}
			Étudier $\int_\R f( t )dt$  revient donc à étudier
			$\int_{\intervalleof{-\infty}{0}} f( t )dt$ et
			$\int_{\intervallefo{0}{+\infty}} f( t )dt$ qui ont la même nature

			Étudions $\int_{\intervallefo{0}{+\infty}} f( t )dt$

			\begin{itemize}
				\item Soit $t \in \intervallefo{1}{+\infty}$. $t < t^2$ donc $-t^2 < -t$ 
				donc $e^{-t^2}< e^{-t}$ 

				\item Soit $x \in \intervalleoo{1}{+\infty}$. $\lim_{x \to +\infty}
				\int_{1}^{x}e^{-t}dt = \lim_{x \to +\infty} ( e^{-1}-e^{-x}
				)=\frac{1}{e}$ 

				Par le théorème de comparaison, $\int_{0}^{+\infty} e^{-t}dt $ converge
				donc $\int_{0}^{+\infty} e^{-t^2}dt $ converge
			\end{itemize}

			Par parité de $f$ sur $\R$, $\int_{-\infty}^{0} e^{-t^2} $ converge. Donc
			$\int_{-\infty}^{+\infty} e^{-t^2}dt $ converge

			\item $\int_{1}^{+\infty} \left( \frac{\sin(t)}{t} \right)^2 dt $
			\begin{align*}
				f: \intervallefo{1}{+\infty} &\longrightarrow \R \\
				t &\longmapsto \left( \frac{\sin(t)}{t} \right)^2 
			.\end{align*}
			$f$ est :
			\begin{itemize}
				\item localement intégrable car continue sur $\intervallefo{1}{+\infty}$ 
				\item à valeurs positives
			\end{itemize}
			Soit $t \in \intervallefo{1}{+\infty}. 0 \le \sin^2(t)$ donc $0 \le \left(
			\frac{\sin(t)}{t} \right)^2 \le \frac{1}{t^2}$ 

			Or $\lim_{x \to +\infty} \int_{1}^{x} \frac{1}{t^2}dt=1 $
			
			Par le théorème de comparaison, $\int_{1}^{+\infty} \frac{1}{t^2}dt $ converge
			donc $\int_{1}^{+\infty} f( t )dt $ converge
		\end{enumerate}
	\end{exo}

	\subsection{Théorème des équivalents}%
	\label{sub:theoreme_des_equivalents}
	
	\begin{theo}
		\textbf{Théorème des équivalents} 

		\textbf{Rappel} :
		\begin{itemize}
			\item Soient $f,g$ deux applications définies sur le même domaine
			$\intervallefo{a}{b}$.

			On dit que $f$ est équivalente à $g$ au voisinage de $b$ si et seulement si il
			existe $U$, un voisinage de $b$, $\eta: U \longrightarrow \R$ telle que :
			\begin{align*}
				\left\{
				\begin{NiceMatrix}
					\forall t \in U \cap \intervallefo{a}{b}, f( t )=g( t )( 1+\eta( t )
					)\\
					\lim_{t \to b} \eta( t )=0
				\end{NiceMatrix}
				\right.
			\end{align*}

			\item La relation est réflexive, symétrique, transitive	
		\end{itemize}

		\begin{itemize}
			\item Soient $a \in \R, b \in \R \cup \set{ +\infty } $ 
			\item Soient $f,g$ localement intégrables sur $\intervallefo{a}{b}$
		\end{itemize}
		S'il existe $c \in \intervallefo{a}{b}$ telle que $
		\left\{
		\begin{NiceMatrix}
			\forall t \in \intervallefo{c}{b}, 0 \le g( t )\\
			f\undersim{ b } g
		\end{NiceMatrix}
		\right.$ alors $\int_{a}^{b} f( t )dt $ converge \ssi $\int_{a}^{b} g( t )dt $
		converge
	\end{theo}
	\begin{demo}
		Supposons l'existence de $c$ et $
		\left\{
		\begin{NiceMatrix}
			g \text{ positive sur } \intervallefo{c}{b}\\
			f \undersim{ b } g
		\end{NiceMatrix}
		\right.$
		
		Il existe donc $\eta : U \longrightarrow \R$ telle que $\lim_{t \to b} \eta ( t
		)=0$, $U$ voisinage de $b$

		Fixons $\epsilon=\frac{1}{2}$ 
		\begin{align*}
			\abs{\eta( t )-0} < \frac{1}{2} \iff& -\frac{1}{2} < \eta( t ) < \frac{1}{2}\\
			\iff& \frac{1}{2} < 1+\eta( t )< \frac{3}{2}
		\end{align*}
		Soit $t \in U \cap \intervallefo{a}{b}$. Multiplions par $g( t )\ge 0$. Par
		hypothèse :
		\begin{align*}
			&\frac{g( t )}{2} < g( t )( 1+\eta( t ) ) < \frac{3}{2}g( t )\\
			\donc& 0 \le \frac{g( t )}{2} < f( t ) < \frac{3}{2}g( t )
		\end{align*}
		Appliquons le théorème de comparaison :
		\begin{align*}
			\int_{a}^{b} f( t )dt \et \int_{a}^{b} g( t )dt  
		\end{align*}
		sont de même nature
	\end{demo}
\end{document}
