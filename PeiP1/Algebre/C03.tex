\input{preamble}

\begin{document}
	\includecourse

	\section{Généralités}%
	\label{sec:generalites}

	\setcounter{subsection}{2}
	\subsection{Prolongement et restriction d'une application}%
	\label{sub:prolongement_et_restriction_d_une_application}

	\begin{expl}
		\textbf{Absence d'unicité du prolongement :}

		Soit \begin{align*}
			f: \intervallefo{0}{+\infty} &\longrightarrow \R \\
			x &\longmapsto x
		\end{align*}

		Soit \begin{align*}
			g: \R &\longrightarrow \R \\
			x &\longmapsto x
		\end{align*}

		Soit \begin{align*}
			h: \R &\longrightarrow \R \\
			x &\longmapsto \abs{x}
		\end{align*}

		$g$ et $h$ sont toutes les deux des prolongements de  $f$ à $\R$ et $g
		\neq h$
	\end{expl}

	\subsection{Composée de deux applications}%
	\label{sub:composee_de_deux_applications}

	\begin{demo}
		\textbf{Preuve de la proposition 1 page 2 (associativité)}
		\begin{align*}
			&E \xrightarrow{g \circ f} G \xrightarrow{h} H \\
			&E \xrightarrow{h \circ (g \circ f} H
		\end{align*}
		\begin{align*}
			&E \xrightarrow{f} G \xrightarrow{h \circ g} H \\
			&E \xrightarrow{(h \circ g) \circ f} H
		\end{align*}

		Les applications $h \circ (g \circ f)$ et $(h \circ g) \circ f$ ont les mêmes
		ensembles de départ $E$ et d'arrivée $H$.

		Il suffit donc de montrer que :
		$\forall x \in E, (h \circ (g \circ f))(x) = ((h \circ g) \circ f)(x)$

		Soit $x \in E$.

		\begin{align*}
			(h \circ (g \circ  f))(x) &= ((h \circ  g) \circ f)(x) \\
			&= h(g(f(x)))
		\end{align*}

		\begin{align*}
			((h \circ g) \circ f)(x) &= (h \circ g)(f(x)) \\
			&= h(g(f(x)))
		\end{align*}

		Donc $(h \circ (g \circ f))(x) = ((h \circ g) \circ f)(x)$

		\Conclusion : $h \circ (g \circ f) = (h \circ g) \circ f$
	\end{demo}

	\begin{rmq}
		On représente $h \circ g \circ f$ de la façon suivante :

		$E \xrightarrow{f} F \xrightarrow{h} H$
	\end{rmq}

	\ATT La composée n'est pas commutative

	Soit $f: E \longmapsto G$ , $g: F \longmapsto G$  deux applications

	\begin{itemize}
		\item $g \circ f$ est bien définie.

		Lorsque $G \not \subset E, f \circ g$ n'est pas définie.

		\item Lorsque $E = G$, de manière générale, $f \circ g \neq g \circ  f$

		Par exemple $E = F = G = \R$ et $f: x \longmapsto x^2$ et $g: x
		\longmapsto x+1$

		\begin{align*}
			g \circ  f: \R &\xrightarrow{f} \R \xrightarrow{g} \R \\
			x &\longmapsto x^2 \longmapsto x^2 + 1 \\[1em]
			f \circ g: \R &\xrightarrow {g} \R \xrightarrow{f} \R \\
			x &\longmapsto x + 1 \longmapsto (x + 1)^2
		\end{align*}
	\end{itemize}

	\begin{demo}
		\textbf{Preuve de la proposition 2 page 2}

		$f \circ Id_{E}$ et $f$ ont les mêmes ensembles de départ $E$ et d'arrivée $F$.

		Soit $x \in E$.

		\begin{align*}
			(f \circ Id_{E})(x) &= f(Id_{E}(x)) \\
			&= f(x)
		\end{align*}

		\Conclusion : $f \circ Id_{E} = f$
	\end{demo}

	\subsection{Notation indicielle}%
	\label{sub:notation_indicielle}

	\begin{expl}
		La suite réelle $(n^2)_{n \in \N}$ est l'application :
		\begin{align*}
			\N &\longrightarrow \R \\
			n &\longmapsto n^2
		\end{align*}
	\end{expl}

	\subsection{Image d'une application}%
	\label{sub:image_d_une_application}

	\textbf{Image réciproque}

	\ATT Soit $y \in F$.

	Sans hypothèses supplémentaires, $f^{-1}(y)$ n'a pas de sens

	Par contre $f^{-1}(\left\{ y \right\})$ a du sens : ensemble des antécédents de $y$
	par $f$.

	\bigskip

	\textbf{Rédaction} :

	Soit $x \in \intervalleff{-1}{3}$

	\begin{align*}
		x \in f^{-1}(E) \iff& f(x) \in E \\
		\iff& -2 < f(x) \le \frac{1}{2}
	\end{align*}

	\section{Injections, surjections, bijections}%
	\label{sec:injections_surjections_bijections}

	\setcounter{subsection}{1}

	\subsection{Application réciproque d'une application bijective}%
	\label{sub:application_reciproque_d_une_application_bijective}

	\begin{expl}
		Réciproque de
		\begin{align*}
			f: \intervalleof{-\infty}{0} &\longrightarrow \intervallefo{0}{+\infty} \\
			x &\longmapsto x^2
		\end{align*}
		est
		\begin{align*}
			f^{-1}: \intervallefo{0}{+\infty} &\longrightarrow \intervalleof{-\infty}{0}
			\\
			x &\longmapsto -\sqrt{x}
		\end{align*}

		Preuve : $\forall x \in \intervalleof{-\infty}{0}, \forall y \in
		\intervallefo{0}{+\infty}, y = x^2 \iff x = -\sqrt{y}$
	\end{expl}

	\begin{demo}
		\textbf{Preuve de la proposition 3 page 6}

		Soient $E, F, G$ trois ensembles.

		Soient  $f: E \longmapsto F$ et $g: F \longmapsto G$ deux applications.

		On suppose que $f$ et $g$ sont injectives.

		\begin{addmargin}[1em]{0pt}
		Soient $x, x' \in E$ tels que $(g \circ f)(x) = (g \circ f)(x')$.

		On a donc : $g(f(x)) = g(f(x'))$.

		$g$ étant injective, on en déduit que $f(x) = f(x')$

		$f$ étant injective, on en déduit que $x = x'$.
		\end{addmargin}

		Donc $g \circ f$ est injective.

		\bigskip
		On suppose que $f$ et $g$ sont surjectives.

		\begin{addmargin}[1em]{0pt}
			Soit $z \in G$.
			$g$ étant surjective, $z$ s'écrit $z = g(y)$ où $y \in F$.

			$f$ étant surjective, $y$ s'écrit $y = f(x)$ où $x \in E$.

			D'où $z = g(f(x))$.

			\Finalement $z = (g \circ f)(x)$ où $x \in E$
		\end{addmargin}

		Donc $g \circ f$ est surjective.
	\end{demo}

	\begin{demo}
		\textbf{Preuve de la proposition 4 page 6}

		Soient $E, F, G$ trois ensembles.

		Soient  $f: E \longmapsto F$ et $g: F \longmapsto G$ deux applications.

		On suppose que $g \circ f$ est injective.

		\begin{addmargin}[1em]{0pt}
		Soient $x, x' \in E$ tels que $f(x) = f(x')$

		On a donc : $g(f(x)) = g(f(x'))$.

		C'est à dire que : $(g \circ f)(x) = (g \circ f)(x')$

		$g \circ f$ étant injective, on en déduit que $x = x'$.
		\end{addmargin}

		Donc $f$ est injective.

		\bigskip
		On suppose que $g \circ f$ est surjective.

		\begin{addmargin}[1em]{0pt}
			Soit $z \in G$.
			$g \circ f$ étant surjective, $z$ s'écrit $z = (g \circ f)(x)$ où $x \in E$.

			On a $z = g(f(x))$, posons $y = f(x) \in F$

			Donc $z$ s'écrit $z = g(y)$ où $y \in F$.
		\end{addmargin}

		Donc $g$ est surjective.
	\end{demo}

	\begin{demo}
		\textbf{Preuve de la proposition 5 page 7}

		Soient $E, F$ deux ensembles.

		Soit $f: E \longmapsto F$ une application

		On suppose que $f$ est bijective.

		\begin{addmargin}[1em]{0pt}
			Soit $x \in E$.

			Posons $y = f(x)$.

			Par définition de $f^{-1}$ : $f^{-1}(y) = x$.

			Donc : $(f^{-1} \circ f)(x) = f^{-1}(f(x)) = f^{-1}(y) = x$.
		\end{addmargin}

		On a montré que $\forall  x \in E, (f^{-1} \circ f)(x) = Id_{E}(x)$

		Donc $f^{-1} \circ f = Id_{E}$

		\bigskip

		Soit $y \in F$.

		\begin{addmargin}[1em]{0pt}
		Posons $x = f^{-1}(y)$.

		Par définition de $f^{-1} : f(x) = y$.

		Donc $(f \circ f^{-1})(y) = f(f^{-1}(y)) = f(x) = y$
		\end{addmargin}

		On a montré que : $\forall y \in F, (f \circ f^{-1})(y) = Id_{F}(y)$

		Donc $f \circ f^{-1} = Id_{f}$

		\Finalement : $f^{-1} \circ f = Id_{E} \et f \circ f^{-1} = Id_{F}$
	\end{demo}

	\begin{demo}
		\textbf{Preuve de la proposition 6 page 7}

		Soient $E, F$ deux ensembles.

		Soient $f: E \longmapsto F$ et $g: F \longmapsto E$ deux applications.

		$g \circ f = Id_{E} \et f \circ g = Id_{F}$

		\begin{itemize}
			\item $Id_{E}$ est injective et $g \circ f = Id_E$  donc, d'après la
			proposition 4, $f$ est injective.
			\item $Id_E$ est surjective et $g \circ f = Id_E$, donc, d'après la
			proposition, $g$ est surjective.
			\item $Id_F$ est injective et $f \circ g = Id_F$ donc, d'après la proposition,
			$g$ est injective.
			\item $Id_F$ est surjective et $f \circ g = Id_F$ donc, d'après la
			proposition, $f$ est surjective.
		\end{itemize}

		\Finalement : $f$ et $g$ sont bijectives.

		\begin{align*}
			f^{-1} &= Id_E \circ f^{-1} \\
			&= (g \circ f) \circ f^{-1} \\
			&= g \circ (f \circ f^{-1}) \\
			&= g \circ Id_F \\
			&= g \\[1em]
			g^{-1} &= Id_F \circ g^{-1} \\
			&= (f \circ g) \circ g^{-1} \\
			&= f o(g \circ g^{-1}) \\
			&= f \circ Id_E \\
			&= f
		\end{align*}

		Donc $f^{-1} = g$ et $g^{-1} = f$
	\end{demo}

	\begin{demo}
		\textbf{Preuve de la proposition 7 page 7}

		On suppose que $f$ est bijective.

		D'après la proposition 5, $f^{-1} \circ f = Id_E$ et $f \circ f^{-1} = Id_F$

		D'après la proposition 6, $f$ et $f^{-1}$ sont réciproques l'une de l'autre

		Donc $(f^{-1})^{-1} = f$

		\bigskip

		On suppose que $f$ et $g$ sont bijectives.

		\begin{align*}
			\shortintertext{D'une part :}
			(g \circ f) \circ (f^{-1} \circ  g^{-1}) &= g \circ (f \circ f^{-1}) \circ
			g^{-1} \\
			&= g \circ  Id_F \circ g^{-1} \\
			&= g \circ g^{-1} \\
			&= Id_G
			\shortintertext{D'autre part :}
			(f^{-1} \circ g^{-1}) \circ (g \circ f) &= f^{-1} \circ (g^{-1} \circ g) \circ
			g\\
			&= f^{-1} \circ Id_F \circ f \\
			&= f^{-1} \circ f \\
			&= Id_E
		\end{align*}

		On déduit de la proposition 6 que $g \circ f$ est $f^{-1} \circ g^{-1}$ sont
		bijectives et réciproques l'une de l'autre.

		Donc $g \circ f$ est bijective et $(g \circ f)^{-1} = f^{-1} \circ g^{-1}$
	\end{demo}


\end{document}
